﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDTO
{
    public class BangCapDTO
    {
        private string maBangCap;

        public string MaBangCap
        {
            get { return maBangCap; }
            set { maBangCap = value; }
        }
        private string tenBangCap;

        public string TenBangCap
        {
            get { return tenBangCap; }
            set { tenBangCap = value; }
        }
        public BangCapDTO(string maBangCap, string tenBangCap)
        {
            MaBangCap = maBangCap;
            TenBangCap= tenBangCap;
        }
    }
}
