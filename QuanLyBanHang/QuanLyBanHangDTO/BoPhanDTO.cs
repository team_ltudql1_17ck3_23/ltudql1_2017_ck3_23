﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDTO
{
    public class BoPhanDTO
    {
        private string maBoPhan;

        public string MaBoPhan
        {
            get { return maBoPhan; }
            set { maBoPhan = value; }
        }

        private string tenBoPhan;

        public string TenBoPhan
        {
            get { return tenBoPhan; }
            set { tenBoPhan = value; }
        }
        public BoPhanDTO(string maBoPhan, string tenBoPhan)
        {
            MaBoPhan = maBoPhan;
            TenBoPhan = tenBoPhan;
        }
        public BoPhanDTO() { }
    }
}
