﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDTO
{
   public class KhachHangDTO
    {
        private string maKhachHang;
        private string hoTen;
        private string sDT;
        private string ngaySinh;
        private string ngayLamThe;
        private string soCMND;
        private int biXoa;
        private string maLoaiKH;
        private string maNhanVien;

        public string MaNhanVien
        {
            get
            {
                return maNhanVien;
            }

            set
            {
                maNhanVien = value;
            }
        }

        public string MaKhachHang
        {
            get
            {
                return maKhachHang;
            }

            set
            {
                maKhachHang = value;
            }
        }

        public string HoTen
        {
            get
            {
                return hoTen;
            }

            set
            {
                hoTen = value;
            }
        }

        public string SDT
        {
            get
            {
                return sDT;
            }

            set
            {
                sDT = value;
            }
        }

        public string NgaySinh
        {
            get
            {
                return ngaySinh;
            }

            set
            {
                ngaySinh = value;
            }
        }

        public string NgayLamThe
        {
            get
            {
                return ngayLamThe;
            }

            set
            {
                ngayLamThe = value;
            }
        }

        public string SoCMND
        {
            get
            {
                return soCMND;
            }

            set
            {
                soCMND = value;
            }
        }

        public int BiXoa
        {
            get
            {
                return biXoa;
            }

            set
            {
                biXoa = value;
            }
        }

        public string MaLoaiKH
        {
            get
            {
                return maLoaiKH;
            }

            set
            {
                maLoaiKH = value;
            }
        }

        public KhachHangDTO() { }
        public KhachHangDTO(string maKhachHang, string hoTen, string sDT, string ngaySinh, string ngayLamThe, string soCMND,int biXoa, string maLoaiKH, string maNhanvien )
        {
            maKhachHang = MaKhachHang;
            hoTen = HoTen;
            sDT = SDT;
            ngaySinh = NgaySinh;
            ngayLamThe = NgayLamThe;
            soCMND = SoCMND;
            biXoa = BiXoa;
            maLoaiKH = MaLoaiKH;
            maNhanVien = MaNhanVien;
        }
    }
}
