﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDTO
{
    public class ChucVuDTO
    {
        private string maChucVu;

        public string MaChucVu
        {
            get { return maChucVu; }
            set { maChucVu = value; }
        }
        private string tenChucVu;

        public string TenChucVu
        {
            get { return tenChucVu; }
            set { tenChucVu = value; }
        }
         public ChucVuDTO(string maChucVu, string tenChucVu)
        {
            MaChucVu = maChucVu;
            TenChucVu = tenChucVu;
        }
    }
}
