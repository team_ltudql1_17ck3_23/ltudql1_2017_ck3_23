﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDTO
{
    public class LoaiKhachHangDTO
    {
        private string maLoaiKH;
        private string tenLoaiKH;

        public string MaLoaiKH
        {
            get
            {
                return maLoaiKH;
            }

            set
            {
                maLoaiKH = value;
            }
        }

        public string TenLoaiKH
        {
            get
            {
                return tenLoaiKH;
            }

            set
            {
                tenLoaiKH = value;
            }
        }

        public LoaiKhachHangDTO(string maLoaiKH, string tenLoaiKH)
        {
            MaLoaiKH = maLoaiKH;
            TenLoaiKH = tenLoaiKH;
        }
        public LoaiKhachHangDTO()
        {

        }
    }
}
