﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDTO
{
    public class NhanVienDTO
    {
        private string maNhanVien;

        public string MaNhanVien
        {
            get { return maNhanVien; }
            set { maNhanVien = value; }
        }
        private string hoTen;

        public string HoTen
        {
            get { return hoTen; }
            set { hoTen = value; }
        }
        private string sDT;

        public string SDT
        {
            get { return sDT; }
            set { sDT = value; }
        }
        private string ngaySinh;

        public string NgaySinh
        {
            get { return ngaySinh; }
            set { ngaySinh = value; }
        }
        private string maBangCap;

        public string MaBangCap
        {
            get { return maBangCap; }
            set { maBangCap = value; }
        }
        private string maBoPhan;
        private string maChucVu;

        public string MaChucVu
        {
          get { return maChucVu; }
          set { maChucVu = value; }
        }
        public string MaBoPhan
        {
            get { return maBoPhan; }
            set { maBoPhan = value; }
        }
        private string tenDangNhap;

        public string TenDangNhap
        {
            get { return tenDangNhap; }
            set { tenDangNhap = value; }
        }
        private string matKhau;

        public string MatKhau
        {
            get { return matKhau; }
            set { matKhau = value; }
        }

        public NhanVienDTO(string maNhanVien, string hoTen, string sDT, string ngaySinh,string maBangCap,string maChucVu ,string maBoPhan ,string tenDangNhap,string matKhau )
        {
            MaNhanVien = maNhanVien;
            HoTen = hoTen;
            SDT = sDT;
            NgaySinh = ngaySinh;
            MaBangCap = maBangCap;
            MaChucVu = maChucVu;
            MaBoPhan = maBoPhan;
            TenDangNhap = tenDangNhap;
            MatKhau = matKhau;
        }
        public NhanVienDTO() { }
        
    }
}
