﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDAO
{
    public class ThanhToanDAO
    {
        public DataTable LayGiaSanPham(string maSanPham)
        {
            DataProvider provider = new DataProvider();
            string sql = "SELECT GIABAN FROM SANPHAM WHERE MASANPHAM LIKE '" + maSanPham + "'";
            provider.Connect();
            DataTable dt = provider.Select(CommandType.Text, sql);
            provider.Disconnect();
            return dt;
        }
        public DataTable DanhSachHoaDon()
        {
            DataProvider provider = new DataProvider();
            string sql = "SELECT * FROM HOADON";
            provider.Connect();
            DataTable dt = provider.Select(CommandType.Text, sql);
            provider.Disconnect();
            return dt;
        }

        public DataTable DanhSachChiTietHoaDon()
        {
            DataProvider provider = new DataProvider();
            string sql = "SELECT * FROM CHITIETHOADON";
            provider.Connect();
            DataTable dt = provider.Select(CommandType.Text, sql);
            provider.Disconnect();
            return dt;
        }

        public int ThemHoaDonVaoDB(string ngayLap, float tongTien, string maKhachHang)
        {
            int nRow = 0;

            // lay ma hoa don lon nhat
            DataTable dtMa = DanhSachHoaDon();
            string maMax = dtMa.Rows[dtMa.Rows.Count - 1]["MAHOADON"].ToString();
            int lastIndex = int.Parse(maMax.Substring(2)) + 1;
            string ma = "HD" + lastIndex.ToString("00");

            DataProvider provider = new DataProvider();
            try
            {
                string strSql = "INSERT INTO HOADON VALUES(@MAHOADON, @NGAYLAP, @TONGTIEN, @MAKHACHHANG, @MATTDH)";
                provider.Connect();
                nRow = provider.ExecuteNonQuery(CommandType.Text, strSql,
                            new SqlParameter { ParameterName = "@MAHOADON", Value = ma },
                            new SqlParameter { ParameterName = "@NGAYLAP", Value = ngayLap },
                            new SqlParameter { ParameterName = "@TONGTIEN", Value = tongTien },
                            new SqlParameter { ParameterName = "@MAKHACHHANG", Value = maKhachHang },
                            new SqlParameter { ParameterName = "@MATTDH", Value = "TT1" }
                    );

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                provider.Disconnect();
            }
            return nRow;
        }
        public int ThemChiTietHoaDonVaoDB(int soLuong, float giaBan, string maSanPham)
        {
            int nRow = 0;

            // lay ma chi tiet hoa don lon nhat
            DataTable dtMaCT = DanhSachChiTietHoaDon();
            string maCTMax = dtMaCT.Rows[dtMaCT.Rows.Count - 1]["MACHITIETHD"].ToString();
            int lastIndexCT = int.Parse(maCTMax.Substring(2)) + 1;
            string maCT = "CTHD" + lastIndexCT.ToString("");

            // lay ma hoa don lon nhat
            DataTable dtMa = DanhSachHoaDon();
            string maMax = dtMa.Rows[dtMa.Rows.Count - 1]["MAHOADON"].ToString();
            int lastIndex = int.Parse(maMax.Substring(2));
            string ma = "HD" + lastIndex.ToString("00");

            DataProvider provider = new DataProvider();
            try
            {
                string strSql = "INSERT INTO HOADON VALUES(@MACHITIETHD, @MAHOADON, @SOLUONG, @GIABAN, @MASANPHAM)";
                provider.Connect();
                nRow = provider.ExecuteNonQuery(CommandType.Text, strSql,
                            new SqlParameter { ParameterName = "@MACHITIETHD", Value = maCT },
                            new SqlParameter { ParameterName = "@MAHOADON", Value = ma },
                            new SqlParameter { ParameterName = "@SOLUONG", Value = soLuong },
                            new SqlParameter { ParameterName = "@GIABAN", Value = giaBan },
                            new SqlParameter { ParameterName = "@MASANPHAM", Value = ma }
                    );

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                provider.Disconnect();
            }
            return nRow;
        }
    }
}
