﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDTO;

namespace QuanLyBanHangDAO
{
    public class NhanVienDAO:DataProvider
    {
       
        public DataTable HienThi()
        {
            string sql = "SELECT MANHANVIEN[Ma nhan vien], HOTEN[Ho ten],SDT[SDT], NGAYSINH[Ngay Sinh], MABANGCAP[MaBC], MABOPHAN[MaBP], MACHUCVU[MaCV],TENDANGNHAP[TenDangNhap],MATKHAU[MatKhau] FROM NHANVIEN";
            
            SqlDataAdapter da = new SqlDataAdapter(sql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable LayTatCaNhanVien()
        {
            string sql = "SELECT * FROM NHANVIEN";

            SqlDataAdapter da = new SqlDataAdapter(sql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable KiemTraDangNhap(string username, string password)
        {
            string sql = string.Format("SELECT * FROM NHANVIEN WHERE TENDANGNHAP='{0}' AND MATKHAU='{1}'", username, password);
            SqlDataAdapter da = new SqlDataAdapter(sql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public bool Them(NhanVienDTO Dto)
        {
            try
            {
                connect.Open();
                string sql = string.Format("INSERT INTO NHANVIEN(MANHANVIEN, HOTEN, SDT, NGAYSING,MABANGCAP,MABOPHAN,MACHUCVU,TENDANGNHAP,MATKHAU) VALUES('{0}', N'{1}', '{2}','{3}','{4}', '{5}', '{6}','{7}','{8}')",
                    Dto.MaNhanVien, Dto.HoTen, Dto.SDT, Dto.NgaySinh, Dto.MaBangCap, Dto.MaBoPhan, Dto.MaChucVu, Dto.TenDangNhap, Dto.MatKhau);
                SqlCommand cmd = new SqlCommand(sql, connect);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception a)
            {

            }
            finally
            {
                connect.Close();
            }
            return false;
            
            
        }
    }
}
