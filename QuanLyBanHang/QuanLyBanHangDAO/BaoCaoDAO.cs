﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDAO
{
    public class BaoCaoDAO
    {
        public DataTable LayDoanhThuThang(string m)
        {
            DataProvider provider = new DataProvider();
            string sqlLayDoanhThu = "SELECT SUM(TONGTIEN) FROM HOADON WHERE NGAYLAP LIKE '%/" + m + "/%'";
            provider.Connect();
            DataTable dt = provider.Select(CommandType.Text, sqlLayDoanhThu);
            provider.Disconnect();
            return dt;
        }
        public DataTable LaySanPhamBanChay()
        {
            DataProvider provider = new DataProvider();
            string sqlLaySanPhamBanChay = "SELECT sp.MASANPHAM, sp.TENSANPHAM, sp.SOLUONG, sp.GIABAN FROM SANPHAM sp, CHITIETHOADON cthd WHERE sp.MASANPHAM = cthd.MASANPHAM GROUP BY sp.MASANPHAM, sp.TENSANPHAM, sp.SOLUONG, sp.GIABAN HAVING COUNT(cthd.MASANPHAM) > 0";
            provider.Connect();
            DataTable dt = provider.Select(CommandType.Text, sqlLaySanPhamBanChay);
            provider.Disconnect();
            return dt;
        }
    }
}
