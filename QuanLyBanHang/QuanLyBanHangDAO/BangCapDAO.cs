﻿using QuanLyBanHangDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangDAO
{
    public class BangCapDAO:DataProvider
    {
        public DataTable LayTatCaBangCap()
        {
            string strSql = "SELECT * FROM BANGCAP ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable HienThi()
        {
            string strSql = "SELECT MABANGCAP[Mã Bằng Cấp ], TENBANGCAP[Tên Bằng Cấp ] FROM BANGCAP ORDER BY MABANGCAP desc ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public bool Them(BangCapDTO DTO)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("INSERT INTO BANGCAP(MABANGCAP, TENBANGCAP) VALUES('{0}', N'{1}')", DTO.MaBangCap, DTO.TenBangCap);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool CapNhat(BangCapDTO DTO)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("UPDATE BANGCAP SET TENBANGCAP=N'{0}' WHERE MABANGCAP='{1}'", DTO.TenBangCap, DTO.MaBangCap);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool Xoa(string maBangCap)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("DELETE FROM BANGCAP WHERE MABANGCAP='{0}'", maBangCap);
                SqlCommand cmd = new SqlCommand(strSql, connect);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
    }
}
