﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDTO;
using System.Data.SqlClient;

namespace QuanLyBanHangDAO
{
    public class BoPhanDAO:DataProvider
    {
        public DataTable LayTatCaBoPhan()
        {
            string strSql = "SELECT * FROM BOPHAN ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable HienThi()
        {
            string strSql = "SELECT MABOPHAN[Mã Bộ Phận], TENBOPHAN[Tên Bộ Phận] FROM BOPHAN ORDER BY MABOPHAN desc ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public bool Them(BoPhanDTO DTO)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("INSERT INTO BOPHAN(MABOPHAN, TENBOPHAN) VALUES('{0}', N'{1}')",DTO.MaBoPhan, DTO.TenBoPhan);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool CapNhat(BoPhanDTO DTO)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("UPDATE BOPHAN SET TENBOPHAN=N'{0}' WHERE MABOPHAN='{1}'", DTO.TenBoPhan, DTO.MaBoPhan);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool Xoa(string maBoPhan)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("DELETE FROM BOPHAN WHERE MABOPHAN='{0}'",maBoPhan );
                SqlCommand cmd = new SqlCommand(strSql, connect);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
    }
}
