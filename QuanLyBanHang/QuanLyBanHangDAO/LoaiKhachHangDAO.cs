﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDTO;
using System.Data;
using System.Data.SqlClient;

namespace QuanLyBanHangDAO
{
    public class LoaiKhachHangDAO : DataProvider
    {
        public DataTable LayTatCaLoaiKhachHang()
        {
            string strSql = "SELECT * FROM LOAIKHACHHANG ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable HienThi()
        {
            string strSql = "SELECT MALOAIKH[Mã loại khách hàng], TENLOAIKH[Tên loại khách hàng] FROM LOAIKHACHHANG ORDER BY MALOAIKH desc ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public bool Them(LoaiKhachHangDTO lkh)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("INSERT INTO LOAIKHACHHANG(MALOAIKH, TENLOAIKH) VALUES('{0}', N'{1}')", lkh.MaLoaiKH, lkh.TenLoaiKH);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool CapNhat(LoaiKhachHangDTO lkh)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("UPDATE LOAIKHACHHANG SET TENLOAIKH=N'{0}' WHERE MALOAIKH='{1}'", lkh.TenLoaiKH, lkh.MaLoaiKH);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool Xoa(string maLoaiKH)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("DELETE FROM LOAIKHACHHANG WHERE MALOAIKH='{0}'", maLoaiKH);
                SqlCommand cmd = new SqlCommand(strSql, connect);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public DataTable TimKiem(string tenLoaiKH)
        {
            string strSql = string.Format("SELECT MALOAIKH[Mã loại khách hàng], TENLOAIKH[Tên loại khách hàng] FROM LOAIKHACHHANG " +
                "WHERE TENLOAIKH LIKE('%{0}%')", tenLoaiKH);
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
    }
}
