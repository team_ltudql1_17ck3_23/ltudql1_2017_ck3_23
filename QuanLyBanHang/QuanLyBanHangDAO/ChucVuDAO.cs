﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDTO;
using System.Data;
using System.Data.SqlClient;

namespace QuanLyBanHangDAO 
{
    public class ChucVuDAO :DataProvider
    {
        public DataTable LayTatCaChucVu()
        {
            string strSql = "SELECT * FROM CHUCVU ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable HienThi()
        {
            string strSql = "SELECT MACHUCVU[Mã Chức Vụ], TENCHUCVU[Tên Chức Vụ] FROM CHUCVU ORDER BY MACHUCVU desc ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public bool Them(ChucVuDTO DTO)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("INSERT INTO CHUCVU(MACHUCVU, TENCHUCVU) VALUES('{0}', N'{1}')", DTO.MaChucVu, DTO.TenChucVu);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool CapNhat(ChucVuDTO DTO)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("UPDATE CHUCVU SET TENCHUCVU=N'{0}' WHERE MACHUCVU='{1}'", DTO.TenChucVu, DTO.MaChucVu);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool Xoa(string maChucVu)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("DELETE FROM CHUCVU WHERE MACHUCVU='{0}'", maChucVu);
                SqlCommand cmd = new SqlCommand(strSql, connect);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
    }
}
