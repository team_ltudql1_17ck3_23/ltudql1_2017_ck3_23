﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDTO;
using System.Data;
using System.Data.SqlClient;

namespace QuanLyBanHangDAO
{
    public class KhachHangDAO : DataProvider
    {
        public DataTable LayTatCaKhachHang()
        {
            string strSql = "SELECT * FROM KHACHHANG ";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public DataTable HienThi()
        {
            string strSql = "SELECT MAKHACHHANG[Mã khách hàng], HOTEN[Họ tên], SDT[SDT], NGAYSINH[Ngày sinh], " +
                "NGAYLAMTHE[Ngày làm thẻ], SOCMND[CMND], MALOAIKH[Mã loại KH], MANHANVIEN[Nhân viên làm thẻ] FROM KHACHHANG";
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public bool Them(KhachHangDTO kh)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("INSERT INTO KHACHHANG(MAKHACHHANG, HOTEN, SDT, NGAYSINH, NGAYLAMTHE, SOCMND, BIXOA, " +
                    "MALOAIKH, MANHANVIEN) VALUES('{0}', N'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", kh.MaKhachHang,kh.HoTen,kh.SDT,kh.NgaySinh,
                    kh.NgayLamThe,kh.SoCMND,kh.BiXoa,kh.MaLoaiKH,kh.MaNhanVien);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool CapNhat(LoaiKhachHangDTO lkh)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("UPDATE LOAIKHACHHANG SET TENLOAIKH=N'{0}' WHERE MALOAIKH='{1}'", lkh.TenLoaiKH, lkh.MaLoaiKH);
                SqlCommand command = new SqlCommand(strSql, connect);
                if (command.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public bool Xoa(string maKH)
        {
            try
            {
                connect.Open();
                string strSql = string.Format("DELETE FROM KHACHHANG WHERE MAKHACHHANG='{0}'", maKH);
                SqlCommand cmd = new SqlCommand(strSql, connect);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connect.Close();
            }
            return false;
        }
        public DataTable TimKiem(string tenLoaiKH)
        {
            string strSql = string.Format("SELECT MALOAIKH[Mã loại khách hàng], TENLOAIKH[Tên loại khách hàng] FROM LOAIKHACHHANG " +
                "WHERE TENLOAIKH LIKE('%{0}%')", tenLoaiKH);
            SqlDataAdapter da = new SqlDataAdapter(strSql, connect);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
    }
}
