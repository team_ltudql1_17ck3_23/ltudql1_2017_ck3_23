﻿using System.Data;
using System.Data.SqlClient;

namespace QuanLyBanHangDAO
{
    public class DataProvider
    {
        protected SqlConnection connect = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=QLBANHANG;Integrated Security=True");

        public void Connect()
        {
            connect.Open();
        }

        public void Disconnect()
        {
            connect.Close();
        }
        public int ExecuteNonQuery(CommandType cmtType, string strSql,
                            params SqlParameter[] parameters)
        {
            try
            {
                SqlCommand command = connect.CreateCommand();
                command.CommandText = strSql;
                command.CommandType = cmtType;
                if (parameters != null && parameters.Length > 0)
                    command.Parameters.AddRange(parameters);
                int nRow = command.ExecuteNonQuery();
                return nRow;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        public DataTable Select(CommandType cmtType, string strSQL,
                            params SqlParameter[] parameters)
        {
            SqlCommand command = connect.CreateCommand();
            command.CommandText = strSQL;
            command.CommandType = cmtType;
            if (parameters != null && parameters.Length > 0)
                command.Parameters.AddRange(parameters);

            SqlDataAdapter da = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
    }
}
