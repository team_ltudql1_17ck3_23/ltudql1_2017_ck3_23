﻿using QuanLyBanHangDAO;
using QuanLyBanHangDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangBUS
{
    public class BangCapBUS
    {
        BangCapDAO DAO = new BangCapDAO();
        public DataTable LayTatCaLoaiKhachHang()
        {
            return DAO.LayTatCaBangCap();
        }
        public DataTable HienThi()
        {
            return DAO.HienThi();
        }
        public bool Them(BangCapDTO DTO)
        {
            return DAO.Them(DTO);
        }
        public bool CapNhat(BangCapDTO DTO)
        {
            return DAO.CapNhat(DTO);
        }
        public bool Xoa(string maLoaiKH)
        {
            return DAO.Xoa(maLoaiKH);
        }
    }
}
