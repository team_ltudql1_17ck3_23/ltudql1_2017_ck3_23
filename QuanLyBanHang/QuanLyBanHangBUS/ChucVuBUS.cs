﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDAO;
using QuanLyBanHangDTO;
using System.Data;

namespace QuanLyBanHangBUS
{
    public class ChucVuBUS
    {
        ChucVuDAO DAO = new ChucVuDAO();
        public DataTable LayTatCaLoaiKhachHang()
        {
            return DAO.LayTatCaChucVu();
        }
        public DataTable HienThi()
        {
            return DAO.HienThi();
        }
        public bool Them(ChucVuDTO DTO)
        {
            return DAO.Them(DTO);
        }
        public bool CapNhat(ChucVuDTO DTO)
        {
            return DAO.CapNhat(DTO);
        }
        public bool Xoa(string maLoaiKH)
        {
            return DAO.Xoa(maLoaiKH);
        }
    }
}
