﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDAO;
using QuanLyBanHangDTO;
using System.Data;

namespace QuanLyBanHangBUS
{
    public class NhanVienBUS
    {
        NhanVienDAO dao = new NhanVienDAO();
       
        public DataTable HienThi()
        {
            return dao.HienThi();
        }
        public DataTable LayTatCaNhanVien()
        {
            return dao.LayTatCaNhanVien();
        }
        public DataTable KiemTraDangNhap(string TenDangNhap, string MatKhau)
        {
            return dao.KiemTraDangNhap(TenDangNhap, MatKhau);
        }
        public bool Them(NhanVienDTO Dto)
        {
            return dao.Them(Dto);
        }
    }
}
