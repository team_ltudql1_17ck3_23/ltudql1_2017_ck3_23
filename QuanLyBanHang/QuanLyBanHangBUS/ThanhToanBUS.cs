﻿using QuanLyBanHangDAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangBUS
{
    public class ThanhToanBUS
    {
        public DataTable LayGiaSanPham(string maSanPham)
        {
            ThanhToanDAO dao = new ThanhToanDAO();
            return dao.LayGiaSanPham(maSanPham);
        }

        public int ThemHoaDonVaoDB(string ngayLap, float tongTien, string maKhachHang)
        {
            ThanhToanDAO dao = new ThanhToanDAO();
            return dao.ThemHoaDonVaoDB(ngayLap, tongTien, maKhachHang);
        }
        public int ThemChiTietHoaDonVaoDB(int soLuong, float giaBan, string maSanPham)
        {
            ThanhToanDAO dao = new ThanhToanDAO();
            return dao.ThemChiTietHoaDonVaoDB(soLuong, giaBan, maSanPham);
        }
    }
}
