﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDAO;
using QuanLyBanHangDTO;
using System.Data;

namespace QuanLyBanHangBUS
{
    public class BoPhanBUS
    {
        BoPhanDAO DAO = new BoPhanDAO();
        public DataTable LayTatCaLoaiKhachHang()
        {
            return DAO.LayTatCaBoPhan();
        }
        public DataTable HienThi()
        {
            return DAO.HienThi();
        }
        public bool Them(BoPhanDTO DTO)
        {
            return DAO.Them(DTO);
        }
        public bool CapNhat(BoPhanDTO DTO)
        {
            return DAO.CapNhat(DTO);
        }
        public bool Xoa(string maLoaiKH)
        {
            return DAO.Xoa(maLoaiKH);
        }
    }
}
