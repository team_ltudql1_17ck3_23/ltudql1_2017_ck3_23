﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDAO;
using QuanLyBanHangDTO;
using System.Data;

namespace QuanLyBanHangBUS
{
    public class LoaiKhachHangBUS
    {
        LoaiKhachHangDAO lkhDAO = new LoaiKhachHangDAO();
        public DataTable LayTatCaLoaiKhachHang()
        {
            return lkhDAO.LayTatCaLoaiKhachHang();
        }
        public DataTable HienThi()
        {
            return lkhDAO.HienThi();
        }
        public bool Them(LoaiKhachHangDTO lkhDTO)
        {
            return lkhDAO.Them(lkhDTO);
        }
        public bool CapNhat(LoaiKhachHangDTO lkhDTO)
        {
            return lkhDAO.CapNhat(lkhDTO);
        }
        public bool Xoa(string maLoaiKH)
        {
            return lkhDAO.Xoa(maLoaiKH);
        }
        public DataTable TimKiem(string tenLoaiKH)
        {
            return lkhDAO.TimKiem(tenLoaiKH);
        }
    }
}
