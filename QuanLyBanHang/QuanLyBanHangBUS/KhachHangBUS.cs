﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHangDAO;
using QuanLyBanHangDTO;
using System.Data;

namespace QuanLyBanHangBUS
{
    public class KhachHangBUS
    {
        KhachHangDAO khDAO = new KhachHangDAO();
        public DataTable LayTatCaLoaiKhachHang()
        {
            return khDAO.LayTatCaKhachHang();
        }
        public DataTable HienThi()
        {
            return khDAO.HienThi();
        }
        public bool Them(KhachHangDTO khDTO)
        {
            return khDAO.Them(khDTO);
        }
        //public bool CapNhat(LoaiKhachHangDTO lkhDTO)
        //{
        //    return lkhDAO.CapNhat(lkhDTO);
        //}
        public bool Xoa(string maKH)
        {
            return khDAO.Xoa(maKH);
        }
        //public DataTable TimKiem(string tenLoaiKH)
        //{
        //    return lkhDAO.TimKiem(tenLoaiKH);
        //}
    }
}
