﻿using QuanLyBanHangDAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHangBUS
{
    public class BaoCaoBUS
    {
        public DataTable LayDoanhThuThang(string m)
        {
            BaoCaoDAO dao = new BaoCaoDAO();
            return dao.LayDoanhThuThang(m);
        }
        public DataTable LaySanPhamBanChay()
        {
            BaoCaoDAO dao = new BaoCaoDAO();
            return dao.LaySanPhamBanChay();
        }
    }
}
