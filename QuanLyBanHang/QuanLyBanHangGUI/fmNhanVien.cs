﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHangBUS;
using QuanLyBanHangDTO;

namespace QuanLyBanHangGUI
{
    public partial class fmNhanVien : Form
    {
        NhanVienBUS Bus;
        NhanVienDTO Dto; 
        public fmNhanVien()
        {
            InitializeComponent();
            Bus = new NhanVienBUS();
        }

        private void fmNhanVien_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgvNhanVien_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            DataGridViewRow row = dgvNhanVien.Rows[e.RowIndex];
            txtMaNhanVien.Text = row.Cells[0].Value.ToString();
            txtHoTen.Text = row.Cells[1].Value.ToString();
            txtSDT.Text = row.Cells[2].Value.ToString();
            txtNgaySinh.Text = row.Cells[3].Value.ToString();
            txtMaBangCap.Text = row.Cells[4].Value.ToString();
            txtMaBoPhan.Text = row.Cells[5].Value.ToString();
            txtMaChucVu.Text = row.Cells[6].Value.ToString();
            txtTenDangNhap.Text = row.Cells[7].Value.ToString();
            txtMatKhau.Text = row.Cells[8].Value.ToString();
        }
        private void LoadData()
        {
            DataTable dtNhanVien = Bus.HienThi();
            dgvNhanVien.DataSource = dtNhanVien;
            dgvNhanVien.Sort(dgvNhanVien.Columns[0], ListSortDirection.Descending);
            dgvNhanVien.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvNhanVien.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }
        private void btnThemNhanVien_Click(object sender, EventArgs e)
        {
            if (txtMaNhanVien.Text.Trim() != "" && txtHoTen.Text.Trim() != "" && txtSDT.Text.Trim() != "" && txtNgaySinh.Text.Trim() != "" && txtMaBangCap.Text.Trim() != ""
                 && txtMaBoPhan.Text.Trim() != "" && txtMaChucVu.Text.Trim() != "" && txtTenDangNhap.Text.Trim() != "" && txtMatKhau.Text.Trim() != "")
            { 
               Dto = new NhanVienDTO(txtMaNhanVien.Text, txtHoTen.Text ,txtSDT.Text , txtNgaySinh.Text , txtMaBangCap.Text, txtMaBoPhan.Text ,txtMaChucVu.Text,txtTenDangNhap.Text,txtMatKhau.Text);
               if (Bus.Them(Dto))
                   MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
               else
                   MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);     
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
