﻿namespace QuanLyBanHangGUI
{
    partial class fmDoanhThuThang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbThang = new System.Windows.Forms.TextBox();
            this.btnBaoCaoDoanhThuThang = new System.Windows.Forms.Button();
            this.lbDoanhTHuThangKetQua = new System.Windows.Forms.Label();
            this.btnHuyXemDoanhThuThang = new System.Windows.Forms.Button();
            this.lbKetQuaDoanhThuThang = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Doanh thu tháng:";
            // 
            // tbThang
            // 
            this.tbThang.Location = new System.Drawing.Point(195, 36);
            this.tbThang.Name = "tbThang";
            this.tbThang.Size = new System.Drawing.Size(190, 22);
            this.tbThang.TabIndex = 5;
            // 
            // btnBaoCaoDoanhThuThang
            // 
            this.btnBaoCaoDoanhThuThang.Location = new System.Drawing.Point(409, 28);
            this.btnBaoCaoDoanhThuThang.Name = "btnBaoCaoDoanhThuThang";
            this.btnBaoCaoDoanhThuThang.Size = new System.Drawing.Size(135, 39);
            this.btnBaoCaoDoanhThuThang.TabIndex = 4;
            this.btnBaoCaoDoanhThuThang.Text = "Xem";
            this.btnBaoCaoDoanhThuThang.UseVisualStyleBackColor = true;
            this.btnBaoCaoDoanhThuThang.Click += new System.EventHandler(this.btnBaoCaoDoanhThuThang_Click);
            // 
            // lbDoanhTHuThangKetQua
            // 
            this.lbDoanhTHuThangKetQua.AutoSize = true;
            this.lbDoanhTHuThangKetQua.Location = new System.Drawing.Point(57, 101);
            this.lbDoanhTHuThangKetQua.Name = "lbDoanhTHuThangKetQua";
            this.lbDoanhTHuThangKetQua.Size = new System.Drawing.Size(118, 17);
            this.lbDoanhTHuThangKetQua.TabIndex = 6;
            this.lbDoanhTHuThangKetQua.Text = "Doanh thu tháng:";
            // 
            // btnHuyXemDoanhThuThang
            // 
            this.btnHuyXemDoanhThuThang.Location = new System.Drawing.Point(578, 28);
            this.btnHuyXemDoanhThuThang.Name = "btnHuyXemDoanhThuThang";
            this.btnHuyXemDoanhThuThang.Size = new System.Drawing.Size(112, 39);
            this.btnHuyXemDoanhThuThang.TabIndex = 7;
            this.btnHuyXemDoanhThuThang.Text = "Hủy";
            this.btnHuyXemDoanhThuThang.UseVisualStyleBackColor = true;
            this.btnHuyXemDoanhThuThang.Click += new System.EventHandler(this.btnHuyXemDoanhThuThang_Click);
            // 
            // lbKetQuaDoanhThuThang
            // 
            this.lbKetQuaDoanhThuThang.AutoSize = true;
            this.lbKetQuaDoanhThuThang.Location = new System.Drawing.Point(192, 101);
            this.lbKetQuaDoanhThuThang.Name = "lbKetQuaDoanhThuThang";
            this.lbKetQuaDoanhThuThang.Size = new System.Drawing.Size(16, 17);
            this.lbKetQuaDoanhThuThang.TabIndex = 6;
            this.lbKetQuaDoanhThuThang.Text = "0";
            // 
            // fmDoanhThuThang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 191);
            this.Controls.Add(this.btnHuyXemDoanhThuThang);
            this.Controls.Add(this.lbKetQuaDoanhThuThang);
            this.Controls.Add(this.lbDoanhTHuThangKetQua);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbThang);
            this.Controls.Add(this.btnBaoCaoDoanhThuThang);
            this.Name = "fmDoanhThuThang";
            this.Text = "fmDoanhThuThang";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbThang;
        private System.Windows.Forms.Button btnBaoCaoDoanhThuThang;
        private System.Windows.Forms.Label lbDoanhTHuThangKetQua;
        private System.Windows.Forms.Button btnHuyXemDoanhThuThang;
        private System.Windows.Forms.Label lbKetQuaDoanhThuThang;
    }
}