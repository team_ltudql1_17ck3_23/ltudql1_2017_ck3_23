﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHangBUS;
using QuanLyBanHangDTO;

namespace QuanLyBanHangGUI
{
    public partial class fmKhachHang : Form
    {
        KhachHangBUS BUS;
        KhachHangDTO DTO;
        public fmKhachHang()
        {
            InitializeComponent();
            BUS = new KhachHangBUS();
        }

        private void fmKhachHang_Load(object sender, EventArgs e)
        {
            LoadData();
            LoaiKhachHangBUS buslkh = new LoaiKhachHangBUS();
            DataTable dtlkh = buslkh.LayTatCaLoaiKhachHang();
            cbbMaLoaiKH.DataSource = dtlkh;
            cbbMaLoaiKH.DisplayMember = "TENLOAIKH";
            cbbMaLoaiKH.ValueMember = "MALOAIKH";

            NhanVienBUS busnv = new NhanVienBUS();
            DataTable dtnv = busnv.LayTatCaNhanVien();
            cbbMaNV.DataSource = dtnv;
            cbbMaNV.DisplayMember = "HOTEN";
            cbbMaNV.ValueMember = "MANHANVIEN";
        }
        private void LoadData()
        {
            DataTable dtKhachHang = BUS.HienThi();
            dgvKhachHang.DataSource = dtKhachHang;
            dgvKhachHang.Sort(dgvKhachHang.Columns[0], ListSortDirection.Ascending);
            dgvKhachHang.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvKhachHang.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }
        private void ReLoad()
        {
            LoadData();
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMaKH.Text.Trim() != "" && txtHoTen.Text.Trim() != "" && txtSDT.Text.Trim() != "" && cbbMaLoaiKH.Text.Trim() != "" && cbbMaNV.Text.Trim() != "")
            {
                try
                {
                    DTO = new KhachHangDTO(txtMaKH.Text,txtHoTen.Text,txtSDT.Text,txtNgaySinh.Text,txtNgayLamThe.Text,txtCMND.Text,0,
                        cbbMaLoaiKH.SelectedValue.ToString(),cbbMaNV.SelectedValue.ToString());
                    if (BUS.Them(DTO))
                        MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMaKH.Clear();
                    txtHoTen.Clear();
                }
            }
            else
            {
                MessageBox.Show("Không được để trống tường này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaKH.Text.Trim() != "")
            {
                try
                {
                    if (BUS.Xoa(txtMaKH.Text))
                        MessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một hàng trong danh sách!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dgvKhachHang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgvKhachHang.Rows[e.RowIndex];
            txtMaKH.Text = row.Cells[0].Value.ToString();
            txtHoTen.Text = row.Cells[1].Value.ToString();
            txtSDT.Text = row.Cells[2].Value.ToString();
            txtNgaySinh.Text = row.Cells[3].Value.ToString();
            txtNgayLamThe.Text = row.Cells[4].Value.ToString();
            txtCMND.Text = row.Cells[5].Value.ToString();
            cbbMaLoaiKH.Text = row.Cells[6].Value.ToString();
            cbbMaNV.Text = row.Cells[7].Value.ToString();
        }
    }
}
