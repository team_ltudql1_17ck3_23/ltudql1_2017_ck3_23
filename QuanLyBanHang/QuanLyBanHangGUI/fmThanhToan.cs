﻿using QuanLyBanHangBUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHangGUI
{
    public partial class fmThanhToan : Form
    {
        public fmThanhToan()
        {
            InitializeComponent();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                ThanhToanBUS bus = new ThanhToanBUS();
                DataTable dt = bus.LayGiaSanPham(txtMaSP.Text);

                int stt = dgvThanhToan.Rows.Count;
                dgvThanhToan.Rows.Add(stt, txtMaSP.Text, txtSoLuong.Text, int.Parse(dt.Rows[0][0].ToString())* int.Parse(txtSoLuong.Text));
            }
            catch
            {
                MessageBox.Show("Loi nhap!");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dgvThanhToan.SelectedRows)
            {
                dgvThanhToan.Rows.RemoveAt(item.Index);
            }
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            ThanhToanBUS bus = new ThanhToanBUS();
            float tongTien = 0;
            if (dgvThanhToan.Rows.Count > 0)
            {
                for (int i = 0; i < dgvThanhToan.Rows.Count; i++)
                {
                    tongTien += float.Parse(dgvThanhToan.Rows[i].Cells[3].Value.ToString());
                }
                int nRow = bus.ThemHoaDonVaoDB(dtpNgayLap.Text.ToString(), tongTien, txtMaKhachHang.Text);

                /*
                for(int i = 0; i < dgvThanhToan.Rows.Count; i++)
                {
                    int x = bus.ThemChiTietHoaDonVaoDB(1, float.Parse(dgvThanhToan.Rows[i].Cells[3].Value.ToString()), dgvThanhToan.Rows[i].Cells[1].Value.ToString());
                }
                */
            }
            else
            {
                MessageBox.Show("Chua them san pham vao danh sach!!");
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
