﻿namespace QuanLyBanHangGUI
{
    partial class FmGiaoDienQuanLy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mụcThôngTinNhânViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuanLyNhanVienToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuanLyBoPhanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuanLyChucVuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuanLyBangCapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doanhThuThángToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sảnPhẩmBánChạyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hóaĐơnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mụcThôngTinNhânViênToolStripMenuItem,
            this.báoCáoToolStripMenuItem,
            this.hóaĐơnToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(975, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mụcThôngTinNhânViênToolStripMenuItem
            // 
            this.mụcThôngTinNhânViênToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.QuanLyNhanVienToolStripMenuItem,
            this.QuanLyBoPhanToolStripMenuItem,
            this.QuanLyChucVuToolStripMenuItem,
            this.QuanLyBangCapToolStripMenuItem});
            this.mụcThôngTinNhânViênToolStripMenuItem.Name = "mụcThôngTinNhânViênToolStripMenuItem";
            this.mụcThôngTinNhânViênToolStripMenuItem.Size = new System.Drawing.Size(121, 24);
            this.mụcThôngTinNhânViênToolStripMenuItem.Text = "Mục Nhân Viên";
            // 
            // QuanLyNhanVienToolStripMenuItem
            // 
            this.QuanLyNhanVienToolStripMenuItem.Name = "QuanLyNhanVienToolStripMenuItem";
            this.QuanLyNhanVienToolStripMenuItem.Size = new System.Drawing.Size(208, 26);
            this.QuanLyNhanVienToolStripMenuItem.Text = "Quản Lý Nhân Viên";
            this.QuanLyNhanVienToolStripMenuItem.Click += new System.EventHandler(this.QuanLyNhanVienToolStripMenuItem_Click);
            // 
            // QuanLyBoPhanToolStripMenuItem
            // 
            this.QuanLyBoPhanToolStripMenuItem.Name = "QuanLyBoPhanToolStripMenuItem";
            this.QuanLyBoPhanToolStripMenuItem.Size = new System.Drawing.Size(208, 26);
            this.QuanLyBoPhanToolStripMenuItem.Text = "Quản Lý Bộ Phận";
            this.QuanLyBoPhanToolStripMenuItem.Click += new System.EventHandler(this.QuanLyBoPhanToolStripMenuItem_Click);
            // 
            // QuanLyChucVuToolStripMenuItem
            // 
            this.QuanLyChucVuToolStripMenuItem.Name = "QuanLyChucVuToolStripMenuItem";
            this.QuanLyChucVuToolStripMenuItem.Size = new System.Drawing.Size(208, 26);
            this.QuanLyChucVuToolStripMenuItem.Text = "Quản Lý Chức Vụ ";
            this.QuanLyChucVuToolStripMenuItem.Click += new System.EventHandler(this.QuanLyChucVuToolStripMenuItem_Click);
            // 
            // QuanLyBangCapToolStripMenuItem
            // 
            this.QuanLyBangCapToolStripMenuItem.Name = "QuanLyBangCapToolStripMenuItem";
            this.QuanLyBangCapToolStripMenuItem.Size = new System.Drawing.Size(208, 26);
            this.QuanLyBangCapToolStripMenuItem.Text = "Quản Lý Bằng Cấp ";
            this.QuanLyBangCapToolStripMenuItem.Click += new System.EventHandler(this.QuanLyBangCapToolStripMenuItem_Click);
            // 
            // báoCáoToolStripMenuItem
            // 
            this.báoCáoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doanhThuThángToolStripMenuItem,
            this.sảnPhẩmBánChạyToolStripMenuItem});
            this.báoCáoToolStripMenuItem.Name = "báoCáoToolStripMenuItem";
            this.báoCáoToolStripMenuItem.Size = new System.Drawing.Size(77, 24);
            this.báoCáoToolStripMenuItem.Text = "Báo Cáo";
            // 
            // doanhThuThángToolStripMenuItem
            // 
            this.doanhThuThángToolStripMenuItem.Name = "doanhThuThángToolStripMenuItem";
            this.doanhThuThángToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.doanhThuThángToolStripMenuItem.Text = "Doanh thu tháng";
            this.doanhThuThángToolStripMenuItem.Click += new System.EventHandler(this.doanhThuThángToolStripMenuItem_Click);
            // 
            // sảnPhẩmBánChạyToolStripMenuItem
            // 
            this.sảnPhẩmBánChạyToolStripMenuItem.Name = "sảnPhẩmBánChạyToolStripMenuItem";
            this.sảnPhẩmBánChạyToolStripMenuItem.Size = new System.Drawing.Size(213, 26);
            this.sảnPhẩmBánChạyToolStripMenuItem.Text = "Sản phẩm bán chạy";
            this.sảnPhẩmBánChạyToolStripMenuItem.Click += new System.EventHandler(this.sảnPhẩmBánChạyToolStripMenuItem_Click);
            // 
            // hóaĐơnToolStripMenuItem
            // 
            this.hóaĐơnToolStripMenuItem.Name = "hóaĐơnToolStripMenuItem";
            this.hóaĐơnToolStripMenuItem.Size = new System.Drawing.Size(97, 24);
            this.hóaĐơnToolStripMenuItem.Text = "Thanh Toán";
            this.hóaĐơnToolStripMenuItem.Click += new System.EventHandler(this.hóaĐơnToolStripMenuItem_Click);
            // 
            // FmGiaoDienQuanLy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 489);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FmGiaoDienQuanLy";
            this.Text = "FmGiaoDienQuanLy";
            this.Load += new System.EventHandler(this.FmGiaoDienQuanLy_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mụcThôngTinNhânViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QuanLyNhanVienToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QuanLyBoPhanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QuanLyChucVuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QuanLyBangCapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doanhThuThángToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sảnPhẩmBánChạyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hóaĐơnToolStripMenuItem;
    }
}