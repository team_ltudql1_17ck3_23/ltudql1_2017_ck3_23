﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHangBUS;

namespace QuanLyBanHangGUI
{
    public partial class fmDangNhap : Form
    {
        NhanVienBUS busNhanVien;
        public fmDangNhap()
        {
            InitializeComponent();
            busNhanVien = new NhanVienBUS();
            GiaoDien();
        }
        private void GiaoDien()
        {
            txtTenDangNhap.Focus();

            txtTenDangNhap.TabIndex = 0;
            txtMatKhau.TabIndex = 1;
            btnDangNhap.TabIndex = 2;
            btnThoat.TabIndex = 3;

            txtMatKhau.UseSystemPasswordChar = true;

            AcceptButton = btnDangNhap;
            CancelButton = btnThoat;

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (txtTenDangNhap.Text != "" && txtMatKhau.Text != "")
            {
                DataTable dtNhanVien = busNhanVien.KiemTraDangNhap(txtTenDangNhap.Text, txtMatKhau.Text);
                if (dtNhanVien.Rows.Count > 0)
                {
                    
                    Form frm = new FmGiaoDienQuanLy();
                    this.Hide();
                    frm.ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Đăng nhập không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void fmDangNhap_Load(object sender, EventArgs e)
        {

        }
    }
}
