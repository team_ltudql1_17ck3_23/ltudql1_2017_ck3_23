﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHangGUI
{
    public partial class FmGiaoDienQuanLy : Form
    {
        private DataRow row;

        public FmGiaoDienQuanLy()
        {
            InitializeComponent();
        }
        private bool CheckExistForm(string name)
        {
            bool check = false;
            foreach (Form frm in this.MdiChildren)
            {
                if (frm.Name == name)
                {
                    check = true;
                    break;
                }
            }
            return check;
        }
        private void ActiveChildForm(string name)
        {
            foreach (Form frm in this.MdiChildren)
            {
                if (frm.Name == name)
                {
                    frm.Activate();
                    break;
                }
            }
        }
        public FmGiaoDienQuanLy(DataRow row)
        {
            this.row = row;
        }

        private void QuanLyNhanVienToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckExistForm("fmNhanVien"))
            {
                fmNhanVien frm = new fmNhanVien();
                frm.MdiParent = this;
                frm.Show();

            }
            else
                ActiveChildForm("fmNhanVien");
        }

        private void FmGiaoDienQuanLy_Load(object sender, EventArgs e)
        {

        }

        private void QuanLyBoPhanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckExistForm("fmBoPhan"))
            {
                fmBoPhan frm = new fmBoPhan();
                frm.MdiParent = this;
                frm.Show();

            }
            else
                ActiveChildForm("fmBoPhan");
        }

        private void QuanLyChucVuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckExistForm("fmChucVu"))
            {
                fmChucVu frm = new fmChucVu();
                frm.MdiParent = this;
                frm.Show();

            }
            else
                ActiveChildForm("fmChucVu");
        }

        private void QuanLyBangCapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckExistForm("fmBangCap"))
            {
                fmBangCap frm = new fmBangCap();
                frm.MdiParent = this;
                frm.Show();

            }
            else
                ActiveChildForm("fmBangCap");
        }

        private void doanhThuThángToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmDoanhThuThang frm = new fmDoanhThuThang();

            frm.Show();
        }

        private void sảnPhẩmBánChạyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmSanPhamBanChay frm = new fmSanPhamBanChay();
            frm.Show();
        }

        private void hóaĐơnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fmThanhToan frm = new fmThanhToan();
            frm.Show();
        }
    }
}
