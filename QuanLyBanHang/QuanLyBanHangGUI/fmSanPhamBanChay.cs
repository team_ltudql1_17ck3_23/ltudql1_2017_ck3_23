﻿using QuanLyBanHangBUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHangGUI
{
    public partial class fmSanPhamBanChay : Form
    {
        public fmSanPhamBanChay()
        {
            InitializeComponent();
        }

        private void fmSanPhamBanChay_Load(object sender, EventArgs e)
        {
            BaoCaoBUS bus = new BaoCaoBUS();

            DataTable dt = bus.LaySanPhamBanChay();
            for(int i = 0; i < dt.Rows.Count; i++)
            {
                dgvBaoCaoBanChay.Rows.Add(dt.Rows[i][0].ToString(), dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString(), dt.Rows[i][3].ToString());
            }
        }
    }
}
