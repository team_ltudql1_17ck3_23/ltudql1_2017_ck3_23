﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHangDTO;
using QuanLyBanHangBUS;

namespace QuanLyBanHangGUI
{
    public partial class fmBoPhan : Form
    {
        BoPhanBUS Bus;
        BoPhanDTO Dto;
        public fmBoPhan()
        {
            InitializeComponent();
            Bus = new BoPhanBUS();
        }

        private void fmBoPhan_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        public void LoadData()
        {
            DataTable dt = Bus.HienThi();
            dgvBoPhan.DataSource = dt;
            dgvBoPhan.Sort(dgvBoPhan.Columns[0], ListSortDirection.Ascending);
            dgvBoPhan.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvBoPhan.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }

        private void dgvBoPhan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgvBoPhan.Rows[e.RowIndex];
            txtMaBoPhan.Text = row.Cells[0].Value.ToString();
            txtTenBoPhan.Text = row.Cells[1].Value.ToString();
        }
        public void ReLoad()
        {
            LoadData();
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {

            if (txtMaBoPhan.Text.Trim() != "" && txtTenBoPhan.Text.Trim() != "")
            {
                try
                {
                    Dto = new BoPhanDTO(txtMaBoPhan.Text, txtTenBoPhan.Text);
                    if (Bus.CapNhat(Dto))
                        MessageBox.Show("Sửa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Sửa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sửa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMaBoPhan.Clear();
                    txtTenBoPhan.Clear();
                }
            }
            else
            {
                MessageBox.Show("Không được để trống tường này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnThemBoPhan_Click(object sender, EventArgs e)
        {
            if (txtMaBoPhan.Text.Trim() != "" && txtTenBoPhan.Text.Trim() != "")
            {
                try
                {
                    Dto = new BoPhanDTO(txtMaBoPhan.Text, txtTenBoPhan.Text);
                    if (Bus.Them(Dto))
                        MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMaBoPhan.Clear();
                    txtTenBoPhan.Clear();
                }
            }
            else
            {
                MessageBox.Show("Không được để trống tường này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaBoPhan.Text.Trim() != "")
            {
                try
                {
                    if (Bus.Xoa(txtMaBoPhan.Text))
                        MessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMaBoPhan.Clear();
                    txtTenBoPhan.Clear();
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một hàng trong danh sách!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}
