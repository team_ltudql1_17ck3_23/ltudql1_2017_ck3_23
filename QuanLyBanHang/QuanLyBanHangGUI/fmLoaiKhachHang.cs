﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHangBUS;
using QuanLyBanHangDTO;


namespace QuanLyBanHangGUI
{
    public partial class fmLoaiKhachHang : Form
    {
        LoaiKhachHangBUS lkhBUS;
        LoaiKhachHangDTO lkhDTO;
        public fmLoaiKhachHang()
        {
            InitializeComponent();
            lkhBUS = new LoaiKhachHangBUS();

        }
        public void LoadData()
        {
            DataTable dt = lkhBUS.HienThi();
            dgvQLLKH.DataSource = dt;
            dgvQLLKH.Sort(dgvQLLKH.Columns[0], ListSortDirection.Ascending);
            dgvQLLKH.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvQLLKH.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }
        public void ReLoad()
        {
            LoadData();
        }
      

     

       

        private void fmLoaiKhachHang_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgvQLLKH_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgvQLLKH.Rows[e.RowIndex];
            txtMaLoaiKH.Text = row.Cells[0].Value.ToString();
            txtTenLoaiKH.Text = row.Cells[1].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMaLoaiKH.Text.Trim() != "" && txtTenLoaiKH.Text.Trim() != "")
            {
                try
                {
                    lkhDTO = new LoaiKhachHangDTO(txtMaLoaiKH.Text, txtTenLoaiKH.Text);
                    if (lkhBUS.Them(lkhDTO))
                        MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMaLoaiKH.Clear();
                    txtTenLoaiKH.Clear();
                }
            }
            else
            {
                MessageBox.Show("Không được để trống tường này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaLoaiKH.Text.Trim() != "")
            {
                try
                {
                    if (lkhBUS.Xoa(txtMaLoaiKH.Text))
                        MessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMaLoaiKH.Clear();
                    txtTenLoaiKH.Clear();
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một hàng trong danh sách!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtMaLoaiKH.Text.Trim() != "" && txtTenLoaiKH.Text.Trim() != "")
            {
                try
                {
                    lkhDTO = new LoaiKhachHangDTO(txtMaLoaiKH.Text, txtTenLoaiKH.Text);
                    if (lkhBUS.CapNhat(lkhDTO))
                        MessageBox.Show("Sửa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Sửa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sửa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMaLoaiKH.Clear();
                    txtTenLoaiKH.Clear();
                }
            }
            else
            {
                MessageBox.Show("Không được để trống tường này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            dgvQLLKH.DataSource = lkhBUS.TimKiem(txtTimKiem.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
