﻿using QuanLyBanHangBUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHangGUI
{
    public partial class fmDoanhThuThang : Form
    {
        public fmDoanhThuThang()
        {
            InitializeComponent();
        }

        private void btnBaoCaoDoanhThuThang_Click(object sender, EventArgs e)
        {
            BaoCaoBUS bus = new BaoCaoBUS();

            string thang = tbThang.Text;
            DataTable dt = bus.LayDoanhThuThang(thang);
            lbDoanhTHuThangKetQua.Text = "Doanh thu tháng " + thang + ": ";
            lbKetQuaDoanhThuThang.Text = dt.Rows[0][0].ToString();
        }

        private void btnHuyXemDoanhThuThang_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
