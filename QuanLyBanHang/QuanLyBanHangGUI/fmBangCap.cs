﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHangBUS;
using QuanLyBanHangDTO;
namespace QuanLyBanHangGUI
{
    public partial class fmBangCap : Form
    {
        BangCapBUS Bus;
        BangCapDTO Dto;
        public fmBangCap()
        {
            InitializeComponent();
            Bus = new BangCapBUS();
        }
        public void LoadData()
        {
            DataTable dt = Bus.HienThi();
            dgvChucVu.DataSource = dt;
            dgvChucVu.Sort(dgvChucVu.Columns[0], ListSortDirection.Ascending);
            dgvChucVu.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvChucVu.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
        }
        public void ReLoad()
        {
            LoadData();
        }
        private void fmBangCap_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMa.Text.Trim() != "" && txtTen.Text.Trim() != "")
            {
                try
                {
                    Dto = new BangCapDTO(txtMa.Text, txtTen.Text);
                    if (Bus.Them(Dto))
                        MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Thêm không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMa.Clear();
                    txtTen.Clear();
                }
            }
            else
            {
                MessageBox.Show("Không được để trống tường này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtMa.Text.Trim() != "" && txtTen.Text.Trim() != "")
            {
                try
                {
                    Dto = new BangCapDTO(txtMa.Text, txtTen.Text);
                    if (Bus.CapNhat(Dto))
                        MessageBox.Show("Sửa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Sửa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sửa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMa.Clear();
                    txtTen.Clear();
                }
            }
            else
            {
                MessageBox.Show("Không được để trống tường này!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMa.Text.Trim() != "")
            {
                try
                {
                    if (Bus.Xoa(txtMa.Text))
                        MessageBox.Show("Xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Xóa không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ReLoad();
                    txtMa.Clear();
                    txtTen.Clear();
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một hàng trong danh sách!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvChucVu_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dgvChucVu.Rows[e.RowIndex];
            txtMa.Text = row.Cells[0].Value.ToString();
            txtTen.Text = row.Cells[1].Value.ToString();
        }

    }
}
